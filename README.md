# Zipdev PHP Code Challenge



### About the challenge

Create a CRUD (API) where you will register, phone numbers in a phone book, you need to save the following fields:
First name.
Surnames.
Phones: N telephones can be stored.
Email: N Emails can be saved.
You must use MySQL, ensuring the integrity of every transaction (add dump file to the code)

You should make only the Endpoints of a REST API, which will be consumed through POSTMAN for its evaluation.

The use of PHP Framework is not allowed.

Nice to Have:
Save the photo of contacts.
Create a call to filter the contacts for each of the fields: Name, Surname, any of the captured phones and/or any of the captured emails
### Solution
##### Installation

This solution is built in PHP7+.

```sh
$ cd zipdev
$ composer install
```

Run test.

```sh
$ ./vendor/bin/phpunit
```
Note about tests: There are the env.php and env.test.php files in the root of the project. Those trying to behave like .env Laravel Files.

env.php file is used for production and Feature test. I used guzzle third-party with PHPUnit in order to test a real response from a real server. This project is not so sophisticated as Laravel framework so, you will need to truncate production database to get all test fully passed.

env.test.php is used only for Unit test. You will need to create a clean database. All necessary stuff will create automatically by those test.

##### Server configuration
`/public` folder is the only should be addressed to apache server.

### Real testing
You can use the url http://zipdev.freengers.com

### Get a list of all current notebooks

**URL** : `/api/v1/notebook`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Response example**



```json
{
    "data": [
          {
            "id": 3,
            "firstname": "Evelyn",
            "surname": "Ramirez",
            "email": [
              "jfcr2204@hotmail.com",
              "evelyn-26a@hotmail.com"
            ],
            "phones": [],
            "photo": null
        }
    ],
    "message": ""
}
```
### Search through notebooks

**URL** : `/api/v1/notebook?query=some text`

**Method** : `GET`

It searches through all fields even with incomplete strings or more than one word. Response as previous.

Example 

http://zipdev.freengers.com/api/v1/notebook?query=ernan%20stil




### Create a notebook

**URL** : `/api/v1/notebook`

**Method** : `POST`

**Request type** : `json`

#### Success Response

**Code** : `200 OK`

**Request example**

```json
{
                
  "firstname": "Evelyn",
  "surname": "Ramirez",
  "email": [
    "jfcr2204@hotmail.com",
    "evelyn-26a@hotmail.com"
  ],
  "phones": [],            
    
}
```
**Response example**

```json
{
    "data": [
          {
            "id": 3,
            "firstname": "Evelyn",
            "surname": "Ramirez",
            "email": [
              "jfcr2204@hotmail.com",
              "evelyn-26a@hotmail.com"
            ],
            "phones": [],
            "photo": null
        }
    ],
    "message": ""
}
```



### Update a notebook

**URL** : `/api/v1/notebook/_id`

**Method** : `PUT`

**Request type** : `json`

**Parameters** : `_id should be from an existent notebook`

#### Success Response

**Code** : `200 OK`

**Request example /api/v1/notebook/3**

```json
{ 
  "email": [
    "another@email.com"    
  ]              
}
```
**Response example**

```json
{
    "data": [
          {
            "id": 3,
            "firstname": "Evelyn",
            "surname": "Ramirez",
            "email": [
              "another@email.com"
            ],
            "phones": [],
            "photo": null
        }
    ],
    "message": ""
}
```






### Delete a notebook

**URL** : `/api/v1/notebook/_id`

**Method** : `DELETE`

**Request type** : `json`

**Parameters** : `_id should be from an existent notebook`

#### Success Response

**Code** : `200 OK`

**Request example /api/v1/notebook/3**


**Response example**

```json
{
    "data": [],
    "message": "Register deleted successfully."
}
```

##### TODO
*   Upload photo
*   GET service for only one notebook
*   Lot of validators
*   It is just a small test

## Fernando castillo <desarrollo@freengers.com>

| [Personal-Site] 
| [Linkedin] 
| [Resume] 


   [Personal-Site]: <http://freengers.com/>
   [Linkedin]: <https://www.linkedin.com/in/jose-fernando-castillo-rosas-a72542117/>
   [Resume]: <https://www.visualcv.com/fernando-castillo-english/pdf/>   
