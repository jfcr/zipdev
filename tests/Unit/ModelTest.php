<?php
/**
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
declare(strict_types=1);

namespace tests\Unit;
use PHPUnit\Framework\TestCase;
use App\Models\Notebook;
use App\Database\Database;




final class ModelTest extends TestCase{

    public $db, $actual, $expected,$notebook;

    public function setUp()
    {


        $db = new Database;
        $db->clean();
        $db->dump(file_get_contents('dump.sql'));
        $this->db= $db;
    }

    public function tearDown()
    {
        $this->db->clean();
    }

    /**
     * Factory
     */
    private function _create(){


        $this->notebook = new NoteBook;
        $phones = [
            '55 4963 74 22',
            '55 4963 74 23',
            '55 4963 74 24',
        ];
        $this->expected = [
            'id'            =>  1,
            'firstname'     =>  'Fernando',
            'surname'       =>  'Castillo',
            'email'         =>  ['desarrollo@freengers.com'],
            'phones'        =>  $phones,
            'photo'         =>  null
        ];

        $input = [
            'id'            =>  1,
            'firstname'     =>  'Fernando',
            'surname'       =>  'Castillo',
            'email'         =>  'desarrollo@freengers.com',
            'phones'        =>  $phones,
            'photo'         =>  null
        ];

        $this->actual   = $this->notebook->create($input);
    }

    /**
     * Read
     */
    function testReadEmpty(){
        $instance = new NoteBook;
        $this->assertEquals($instance->get(),[]);
    }

    /**
     * Create register
     */
    function testCreate(){
        $this->_create();
        $this->assertEquals($this->expected,$this->actual);
    }


    /**
     * Update
     */
    function testUpdate(){

        $test_new_firstname = 'John';
        $test_new_surname = 'Lennon';
        $this->_create();


        $this
            ->notebook
            ->where('id','=',1)
            ->update([
                'firstname'     => $test_new_firstname,
                'surname'       => $test_new_surname,
            ])

        ;

        $other  = $this->notebook->where('id','=',1)->first();

        $this->assertEquals($test_new_firstname,$other['firstname']);
        $this->assertEquals($test_new_surname,$other['surname']);
    }

    /**
     * Update
     */
    function testDelete()
    {
        $this->_create();
        $this->notebook->delete(1);

        $other  = $this->notebook->where('id','=',1)->first();
        $this->assertEquals(0,$other['id']);
    }

}