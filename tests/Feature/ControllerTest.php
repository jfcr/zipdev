<?php
/**
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
declare(strict_types=1);

namespace tests\Unit;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

final class ControllerTest extends TestCase
{


    function testWrongIndex(){

        $code = null;
        $client = new Client(['base_uri' => getenv('BASE_URI')]);
        try{
            $client->request('GET', 'not_exist');
        } catch (RequestException $e) {
            $code = $e->getCode();
        }

        $this->assertEquals(400,$code);
    }


    function testPost(){
        $options = [
            'json'  =>  [
                'firstname'     =>  'John',
                'surname'       =>  'Lennon',
                'email'         =>  ['john@lennon.com','ringo@starr.com'],
                'phones'        =>  ['444','555','666']
            ]
        ];

        $client = new Client(['base_uri' => getenv('BASE_URI')]);
        $response = $client->request('POST', 'notebook',$options);
        $json = json_decode($response->getBody()->getContents(),true);

        $this->assertEquals('Register created successfully.',$json['message']);
        $this->assertEquals(200,$response->getStatusCode());
    }


    function testUpdate(){
        $options = [
            'json'  =>  [
                'firstname'     =>  'Paul',
                'surname'       =>  'McCartney',
            ]
        ];

        $client = new Client(['base_uri' => getenv('BASE_URI')]);
        $response = $client->request('PUT', 'notebook/1',$options);
        $json = json_decode($response->getBody()->getContents(),true);

        $this->assertEquals('Register updated successfully.',$json['message']);
        $this->assertEquals(200,$response->getStatusCode());
    }


    function testDelete(){

        $client = new Client(['base_uri' => getenv('BASE_URI')]);
        $response = $client->request('DELETE', 'notebook/1');
        $json = json_decode($response->getBody()->getContents(),true);

        $this->assertEquals('Register deleted successfully.',$json['message']);
        $this->assertEquals(200,$response->getStatusCode());
    }

}