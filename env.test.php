<?php
/**
 * ONLY FOR UNIT TEST
 */
    $variables = [

        'DB_HOST'       =>  '127.0.0.1',
        'DB_USERNAME'   =>  'root',
        'DB_PASSWORD'   =>  '',
        'DB_NAME'       =>  'zipdev_test',
        'DB_PORT'       =>  '3306',

        'BASE_URI'      =>  'http://localhost/zipdev/api/v1/'

    ];
foreach ($variables as $key => $value) {
    putenv("$key=$value");
}