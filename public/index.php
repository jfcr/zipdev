<?php
/**
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
require_once __DIR__ . '/../vendor/autoload.php';

use App\Http\Router;

/**
 * Registering Routes
 */
Router::init();

Router::get('notebook','App\Http\NoteBookController@index');
Router::post('notebook','App\Http\NoteBookController@create');
Router::put('notebook/{id}','App\Http\NoteBookController@update');
Router::delete('notebook/{id}','App\Http\NoteBookController@delete');

Router::neither();