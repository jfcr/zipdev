<?php
/**
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
declare(strict_types=1);

namespace App\Models;
use App\Database\Database;
use Exception;

class Notebook
{
    // database connection and table name
    private $conn;
    private $table_name = "phone_books";
    private $query = [];
    private $query_or = [];
    private const ID            = 'id';
    private const FIRSTNAME     = 'firstname';
    private const SURNAME       = 'surname';
    private const EMAIL         = 'email';
    private const PHONES        = 'phones';
    private const PHOTO         = 'photo';




    public function __construct()
    {

        if(is_null($this->conn)){
            $db = new Database;
            $this->conn = $db->conn;
        }
    }

    protected function _build_query(){
        $execution='';
        $tmp        = [];
        $tmp2       = [];
        foreach($this->query as $q){
            $value = $this->conn->real_escape_string((string)$q['value']);
            $tmp[] = " {$q['field']} {$q['comparator']} '{$value}' ";
        }
        if(count($tmp)>0){
            $execution .= " WHERE ".implode(' AN ',$tmp);
        }

        foreach($this->query_or as $q){
            $value = $this->conn->real_escape_string((string)$q['value']);
            $tmp2[] = " {$q['field']} {$q['comparator']} '{$value}' ";
        }

        if(count($tmp2)>0){
            $execution .= " OR ".implode(' OR ',$tmp2);
        }
        return $execution;
    }
    protected function _get(){


        $execution  =   "
                SELECT * FROM {$this->table_name} 
                {$this->_build_query()}
        ";

        //die(var_dump($execution));
        $this->_clean();
        return $this->conn->query($execution,MYSQLI_USE_RESULT);
    }

    protected function _clean(){
        $this->query = [];
    }

    protected function _populate($result){
        return [
            self::ID            =>  (int)$result[0],
            self::FIRSTNAME     =>  $result[1],
            self::SURNAME       =>  $result[2],
            self::EMAIL         =>  isset($result[3]) ? json_decode($result[3]) : [],
            self::PHONES        =>  isset($result[4]) ? json_decode($result[4]) : [],
            self::PHOTO         =>  $result[5],
        ];
    }

    public function get(){
        $result = [];
        foreach($this->_get()->fetch_all() as $r){
            $result[] = $this->_populate($r);
        }
        return $result;
    }

    public function first(){
        $row    = $this->_get()->fetch_row();
        return $this->_populate($row);
    }

    public function where($field,$comparator,$value){
        $this->query[] = ['field'=>$field,'comparator'=>$comparator,'value'=>$value];
        return $this;
    }

    public function or($field,$comparator,$value){
        $this->query_or[] = ['field'=>$field,'comparator'=>$comparator,'value'=>$value];
        return $this;
    }

    protected function _clean_array(array $array,$for_update=false){
        $firstname      = isset($array[self::FIRSTNAME]) ? $array[self::FIRSTNAME] : null;
        $surname        = isset($array[self::SURNAME]) ? $array[self::SURNAME] : null;
        $email          = isset($array[self::EMAIL]) ? $array[self::EMAIL] : null;
        $phones         = isset($array[self::PHONES]) ? $array[self::PHONES] : null;
        $photo          = isset($array[self::PHOTO]) ? $array[self::PHOTO] : null;


        if(($firstname===null || $email===null) && $for_update===false) return false;

        if($phones){

            if(is_array($phones)){
                $phones = json_encode($phones);
            }else{
                $phones = json_encode([$phones]);
            }

        }

        if($email){

            if(is_array($email)){
                $email = json_encode($email);
            }else{
                $email = json_encode([$email]);
            }

        }

        $response = [$firstname,$surname,$email,$phones,$photo];
        if($for_update===true){
            if(is_null($response[0])){
                unset($response[0]);
            }

            if(is_null($response[1])){
                unset($response[1]);
            }

            if(is_null($response[2])){
                unset($response[2]);
            }

            if(is_null($response[3])){
                unset($response[3]);
            }

            if(is_null($response[4])){
                unset($response[4]);
            }
        }
        return $response;

    }

    public function create(array $_array){
        if(count($_array)<=0) return false;

        $clean = $this->_clean_array($_array);


        $stmt = $this->conn->prepare("INSERT INTO {$this->table_name} (firstname, surname, email, phones, photo) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", ...$clean);
        $stmt->execute();

        $this->_clean();
        return $this
            ->where('id','=',$stmt->insert_id)
            ->first()
        ;
    }

    public function update(array $array){

        if(count($this->query)<=0) {
            throw new Exception('Must specify a where clause');
        }

        $update_array = [];
        $bind='';
        $values = $this->_clean_array($array,true);

        foreach($array as $k=>$v){
            $update_array[]     =   "{$k}=?";
            $bind              .=   's';
        }

        $set = implode(",",$update_array);
        $execution  =   "
            UPDATE {$this->table_name}
               SET {$set}
                   {$this->_build_query()}
             ";


        $stmt = $this->conn->prepare($execution);
        $stmt->bind_param($bind, ...$values);
        $response = $stmt->execute();

        $this->_clean();
        return $response;
    }

    public function delete($id){

        $stmt = $this->conn->prepare("DELETE FROM {$this->table_name} WHERE id=?");
        $stmt->bind_param("s", $id);
        $response = $stmt->execute();

        $this->_clean();
        return $response;
    }
}