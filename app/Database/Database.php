<?php
/**
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
declare(strict_types=1);

namespace App\Database;
use mysqli;

class Database{

    // specify your own database credentials
    private $host = "";
    private $db_name = "";
    private $username = "";
    private $auth = "";
    private $port = "";
    public $conn;

    public function __construct()
    {

        if(getenv('APP_ENV')=='testing'){
            require_once ('env.test.php');
        }else{
            require_once ('../env.php');
        }

        $this->host = getenv('DB_HOST');
        $this->db_name = getenv('DB_NAME');
        $this->username = getenv('DB_USERNAME');
        $this->auth = getenv('DB_PASSWORD');
        $this->port = getenv('DB_PORT');

        $this->conn = $this->getConnection();
    }

    /**
     * Create a new mysql connection
     * @return mysqli|null
     */
    public function getConnection(){
        $tmp = null;
        try{
            $tmp = new mysqli($this->host, $this->username, $this->auth,$this->db_name,(int)$this->port);
            $tmp->set_charset("utf8");
        }catch(Exception $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $tmp;
    }


    /**
     * Executes a drop query
     */
    public function clean(){
        $this->conn->query('DROP TABLE  IF EXISTS `phone_books`');
    }

    /**
     * Execute a SQL query
     * @param $txt
     */
    public function dump($txt){
        //$txt = $this->conn->real_escape_string($txt);
        $this->conn->query($txt);
    }
}