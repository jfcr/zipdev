<?php
/**
 * Router controller class from scratch
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
declare(strict_types=1);

namespace App\Http;

class Router
{
    static $u='';
    static private $uri_elements=[];
    static private $query = '';

    /**
     * Check all about request url
     */
    static function init(){
        header('Content-type: application/json');
        $valid=true;
        $script_name = $_SERVER['SCRIPT_NAME'];
        $request_uri = $_SERVER['REQUEST_URI'];

        $dir = str_replace('index.php','',$script_name);
        //$real_request_uri = str_replace($dir,'',$request_uri);
        if($dir!=='/'){
            $real_request_uri = str_replace($dir,'',$request_uri);
        }else{
            $real_request_uri = substr($request_uri,1);
        }

        $query =explode("?",$real_request_uri);
        if(isset($query[1])){
            self::$query = $query[1];
        }

        $uri_elements = explode('/',$query[0]);


        if(count($uri_elements)<2){
            $valid=false;
        }

        if(!isset($uri_elements[0]) || $uri_elements[0]!='api'){
            $valid=false;
        }

        if(!isset($uri_elements[1]) || $uri_elements[1]!='v1'){
            $valid=false;
        }


        self::$uri_elements =   array_slice($uri_elements,2);

        if(!$valid){
            return self::error('End point should be api/v1/');
        }
    }

    /**
     * If any router matches with index.php
     */
    static function neither(){
        return self::error('No routes for your request');
    }

    /**
     * Sends a error message
     * @param $message
     */
    public static function error($message){
        http_response_code(400);
        echo json_encode( ['message'=>$message] );
        exit(1);
    }

    /**
     * Sends a successfully message
     * @param array $data
     * @param string $message
     */
    public static function json(array $data,string $message=''){
        http_response_code(200);
        echo json_encode([
            'data'      =>  $data,
            'message'   =>  $message
        ]);
        exit(0);

    }

    /**
     * Call to the controller and passes request
     * @param string $class
     * @param string $id
     */
    private static function instance(string $class, string $id=''){

        $tmp = explode("@",$class);

        parse_str(self::$query,$request);
        if(!$request){
            $request=[];
        }
        $request = array_merge($request,$_POST);
        $json = json_decode(file_get_contents('php://input'),true);
        if (is_array($json)){
            $request = array_merge($request,$json);
        }

        $object = new $tmp[0]();
        $object->{$tmp[1]}($request,$id);
    }

    /**
     * GET Rest
     * @param string $endpoint
     * @param string $class
     */
    public static function get(string $endpoint,string $class){

        if($_SERVER['REQUEST_METHOD']!=='GET' || $endpoint!==self::$uri_elements[0]) {
            return;
        }
        self::instance($class);

    }


    /**
     * POST Rest
     * @param string $endpoint
     * @param string $class
     */
    public static function post(string $endpoint,string $class){

        if($_SERVER['REQUEST_METHOD']!=='POST' || $endpoint!==self::$uri_elements[0]) {
            return;
        }
        self::instance($class);
    }


    /**
     * PUT Rest
     * @param string $endpoint
     * @param string $class
     */
    public static function put(string $endpoint,string $class){

        if($_SERVER['REQUEST_METHOD']!=='PUT' || $endpoint!==self::$uri_elements[0]."/{id}" || !isset(self::$uri_elements[1])) {
            return;
        }
        self::instance($class,self::$uri_elements[1]);
    }


    /**
     * DELETE Rest
     * @param string $endpoint
     * @param string $class
     */
    public static function delete(string $endpoint,string $class){

        if($_SERVER['REQUEST_METHOD']!=='DELETE' || $endpoint!==self::$uri_elements[0]."/{id}" || !isset(self::$uri_elements[1])) {
            return;
        }
        self::instance($class,self::$uri_elements[1]);
    }

}
