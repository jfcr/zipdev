<?php
/**
 * Crud controller class from scratch
 * @author Fernando Castillo <desarrollo@freengers.com>
 */
namespace App\Http;

use App\Models\Notebook;


class NoteBookController
{

    /**
     * List all notebooks in database
     * @param array $request
     */
    public function index(array $request=[]){

        $notebook = new Notebook;
        if(isset($request['query'])){
            $notebook->where('firstname','=',$request['query']);
            $query = explode(" ",$request['query']);
            foreach($query as $v){
                $notebook->or('firstname','like',"%{$v}%");
                $notebook->or('surname','like',"%{$v}%");
                $notebook->or('email','like',"%{$v}%");
                $notebook->or('phones','like',"%{$v}%");
            }

            $results = $notebook->get();
        }else{
            $results = $notebook->get();
        }

        Router::json($results);
    }

    /**
     * Create a new notebook
     * @param array $request
     */
    public function create(array $request=[]){
        $notebook = new Notebook;
        $response = $notebook->create($request);
        if(!$response){
            Router::error(['message'=>'Parameter firstname and email are required']);
        }else{
            Router::json($response,'Register created successfully.');
        }

    }

    /**
     * Update a notebook
     * @param array $request
     * @param string $id
     * @throws \Exception
     */
    public function update(array $request=[],string $id){
        $notebook = new Notebook;
        $register  = $notebook->where('id','=',(int)$id)->first();
        if($register['id']===0){
            Router::error(['message'=>'Register not found']);
        }else{
            $update = [];

            if(isset($request['firstname'])) {
                $update['firstname'] = $request['firstname'];
            }

            if(isset($request['surname'])) {
                $update['surname'] = $request['surname'];
            }

            if(isset($request['email'])) {
                $update['email'] = $request['email'];
            }

            if(isset($request['phones'])) {
                $update['phones'] = $request['phones'];
            }

            $notebook
                ->where('id','=',(int)$id)
                ->update($update);
            $new_register  = $notebook->where('id','=',(int)$id)->first();
            Router::json($new_register,'Register updated successfully.');
        }
    }

    /**
     * Delete a register
     * @param array $request
     * @param string $id
     */
    public function delete(array $request=[],string $id){

        $notebook = new Notebook;
        $register  = $notebook->where('id','=',(int)$id)->first();
        if($register['id']===0){
            Router::error(['message'=>'Register not found']);
        }else{
            $notebook->delete((int)$id);
            Router::json([],'Register deleted successfully.');
        }

    }

}