CREATE TABLE `phone_books` (
                               `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                               `firstname` varchar(200) DEFAULT NULL,
                               `surname` varchar(200) DEFAULT NULL,
                               `email` text,
                               `phones` text,
                               `photo` varchar(500) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;